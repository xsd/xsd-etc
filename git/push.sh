#! /bin/sh

# Assume this script never run via PATH.
#
self=`realpath $0`
. `dirname $self`/modules

wd=`pwd`
br=$1

for i in $modules; do
  echo "push $i" 1>&2
  cd $i

  if [ -z "$1" ]; then
    br=`git symbolic-ref -q HEAD`
    br=`echo $br | sed -e 's%^refs/heads/%%'`
  fi

  # Also push tags if we are pushing master.
  #
  if [ "$br" = "master" ]; then
    git push --tags origin master
  else
    git push $ops origin $1
  fi

  if [ $? -ne 0 ]; then
    echo "push FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
