#! /bin/sh

# Assume this script never run via PATH.
#
self=`realpath $0`
. `dirname $self`/modules

wd=`pwd`

for i in $modules; do
  echo "stat $i" 1>&2
  cd $i
  git status

  if [ $? -ne 0 ]; then
    echo "stat FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
