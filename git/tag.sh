#! /bin/sh

# Assume this script never run via PATH.
#
self=`realpath $0`
. `dirname $self`/modules

wd=`pwd`

if [ "$1" = "" ]; then
  echo "missing version" 1>&2
  exit 1
fi

for i in $modules; do
  echo "tag $i" 1>&2
  cd $i
  git tag -a $1 -m "Tag version $1"

  if [ $? -ne 0 ]; then
    echo "tag FAILED" 1>&2
    exit 1
  fi

  cd $wd
done
