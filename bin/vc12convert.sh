#! /usr/bin/env bash

# Convert VC11 project files to VC12.
#

trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

wd=`pwd`
vi=vc11
vo=vc12

while [ $# -gt 0 ]; do
  case $1 in
    *)
      error "unknown option: $1"
      exit 1
      ;;
    esac
done

files=`find . -name "*-$vi.vcxproj" -type f`

for p in $files; do

  d=`dirname $p`
  f=`basename $p -$vi.vcxproj`

  cd $d

  cp $f-$vi.vcxproj.filters $f-$vo.vcxproj.filters

  sed \
    -e "s#ToolsVersion=\"4.0\"#ToolsVersion=\"12.0\"#" \
    -e "s#<PlatformToolset>v110#<PlatformToolset>v120#" \
    -e "s#</ClCompile>#  <SDLCheck>true</SDLCheck>\n    </ClCompile>#" \
    $f-$vi.vcxproj >$f-$vo.vcxproj

  todos $f-$vo.vcxproj

  if [ `wc -c <$f-$vi.vcxproj` -eq `wc -c <$f-$vo.vcxproj` ]; then
    error "no changes made in $d"
    cd $wd
    exit 1
  fi

  cd $wd
done
