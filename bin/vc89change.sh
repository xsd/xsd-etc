#! /usr/bin/env bash

# Change VC8 or VC9 project file for another example.
#
# -n <new name>
# -f <old-file>=<new-file> matched with trailing . (must have ext); multiple
# -x <new xsd command and options>
#

trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

function uuid ()
{
  uuidgen | sed -e 's#\(.*\)#\U\1#'
}

new=
xsd=
fop=

while [ $# -gt 0 ]; do
  case $1 in
    -n)
      shift
      new=$1
      shift
      ;;
    -f)
      shift
      o=`echo $1 | sed -e 's#\(.*\)=\(.*\)#\1#'`
      n=`echo $1 | sed -e 's#\(.*\)=\(.*\)#\2#'`
      echo "file change: $o.* -> $n.*"
      fop="$fop -e s/$o\./$n./g"
      shift
      ;;
    -x)
      shift
      xsd=$1
      shift
      ;;
    -*)
      error "unknown option: $1"
      exit 1
      ;;
     *)
      break
      ;;
  esac
done

if [ "$new" = "" ]; then
  error '-n <new name> expected'
  exit 1
fi

if [ "$xsd" = "" ]; then
  error '-x <new xsd command and options> expected'
  exit 1
fi

if [ "$1" = "" ]; then
  error 'input file expected'
  exit 1
fi

old=`echo "$1" | sed -e 's/\(.*\)-[89].*vcproj/\1/'`
ext=`echo "$1" | sed -e 's/.*-\([89].*vcproj\)/\1/'`

echo "old name   : $old"
echo "new name   : $new"
echo "new xsd cmd: $xsd"

sed \
-e "s#Name=\"$old\"#Name=\"$new\"#" \
-e "s#ProjectGUID=\"{.*}\"#ProjectGUID=\"{`uuid`}\"#" \
-e "s#RootNamespace=\"$old\"#RootNamespace=\"$new\"#" \
-e "s#UniqueIdentifier=.*#uuidgen | sed -e 's%\\\\(.*\\\\)%\t\t\tUniqueIdentifier=\"{\\\\U\\\\1}\"%'#e" \
-e "s#CommandLine=\"xsd.exe .* \([^ ][^ ]*\)\"#CommandLine=\"xsd.exe $xsd \1\"#" \
$fop \
$1 >$new-$ext

todos $new-$ext
