@echo off
rem
rem build.bat vc-version conf plat [/Build|/Clean|/Rebuild]
rem
rem conf: {Debug,Release}|all
rem plat: {Win32,x64}|all
rem

setlocal

if "_%1_" == "__" (
  echo no VC++ version specified
  goto usage
)

if "_%~2_" == "__" (
  echo no configuration specified
  goto usage
)

if "_%~3_" == "__" (
  echo no platform specified
  goto usage
)

set "failed="

if "_%1_" == "_8_" set "vcver=8"
if "_%1_" == "_9_" set "vcver=9"
if "_%1_" == "_10_" set "vcver=10"
if "_%1_" == "_11_" set "vcver=11"
if "_%1_" == "_12_" set "vcver=12"

if "_%vcver%_" == "__" (
  echo unknown VC++ version %1
  goto usage
)

set "confs=%~2"
set "plats=%~3"

if "_%confs%_" == "_all_" set "confs=Debug Release"
if "_%plats%_" == "_all_" set "plats=Win32 x64"

set "action=%4"
if "_%action%_" == "__" set "action=/Build"

set "devenv=%DEVENV%"
if "_%devenv%_" == "__" set "devenv=devenv.com"

goto start

rem
rem %1 - solution name
rem %2 - configuration to build
rem %3 - platform to build
rem
:run_build
  echo.
  echo building %1 %3 %2
  "%devenv%" %1 %action% "%2|%3" 2>&1
  if errorlevel 1 set "failed=%failed% %1\%3\%2"
goto :eof

:start

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build examples/cxx/tree/tree-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build examples/cxx/tree/embedded/embedded-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build examples/cxx/tree/custom/custom-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build examples/cxx/tree/custom/calendar/calendar-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

for %%c in (%confs%) do (
  for %%p in (%plats%) do (
    call :run_build examples/cxx/parser/parser-vc%vcver%.sln %%c %%p
  )
)

if not "_%failed%_" == "__" goto error

echo.
echo ALL BUILDS SUCCEEDED
echo.
goto end

:usage
echo.
echo usage: build.bat vc-version conf plat [action]
echo   valid configurations are: {Debug,Release}|all
echo   valid platforms are: {Win32,x64}|all
echo   valid actions are: /Build (default), /Clean, and /Rebuild
echo.

:error
if not "_%failed%_" == "__" (
  echo.
  for %%t in (%failed%) do echo FAILED: %%t
  echo.
)
endlocal
exit /b 1

:end
endlocal
