@echo off

if "_%1_" == "__" (
  echo no VC++ version specified
  goto usage
)

set ROOT=c:\projects
set "DIFF=c:\cygwin\bin\diff.exe -ubB"

if "_%1_" == "_8_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.com"
if "_%1_" == "_9_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.com"
if "_%1_" == "_10_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.com"
if "_%1_" == "_11_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\devenv.com"
if "_%1_" == "_12_" set "DEVENV=c:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.com"

set PATH=%ROOT%\xsd\xsd-i686-windows\bin;%PATH%
set PATH=%ROOT%\xerces-vc\bin;%PATH%
set PATH=%ROOT%\xerces-vc\bin64;%PATH%

if "_%2_" == "__" goto end

%2 %3 %4 %5 %6 %7 %8 %9
goto end

:usage
echo.
echo usage: setenv.bat vc-version ...
echo.

:error
exit /b 1

:end
