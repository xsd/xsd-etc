%ifarch i686
%define arch i686
%endif

%ifarch x86_64
%define arch x86_64
%endif

Summary: XML Schema to C++ data binding compiler
Name: xsd
Version: 4.0.0
Release: 1
License: GPLv2 + exceptions
Group: Development/Tools
URL: http://www.codesynthesis.com/products/xsd/
Source: %{name}-%{version}-%{arch}-linux-gnu.tar.bz2
Prefix: %{_prefix}
Buildroot: %{_tmppath}/%{name}-root
AutoReqProv: no

%description
CodeSynthesis XSD is an XML Schema to C++ data binding compiler.
Provided with an XML instance specification (XML Schema), it
generates C++ classes that represent the given vocabulary as
well as parsing and serialization code.

%prep
%setup -q -n %{name}-%{version}-%{arch}-linux-gnu

%build

# We don't want the binaries to be stripped.
#
%ifarch x86_64
%define __os_install_post /usr/lib/rpm/brp-compress
%endif

%install

rm -rf ${RPM_BUILD_ROOT}

mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{_includedir}
mkdir -p $RPM_BUILD_ROOT%{_docdir}/xsd
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man1

cp -rL bin/* $RPM_BUILD_ROOT%{_bindir}/

cp -rL libxsd/xsd $RPM_BUILD_ROOT%{_includedir}/

cp -rL NEWS $RPM_BUILD_ROOT%{_docdir}/xsd/
cp -rL README $RPM_BUILD_ROOT%{_docdir}/xsd/
cp -rL doc/default.css $RPM_BUILD_ROOT%{_docdir}/xsd/
cp -rL doc/xsd.xhtml $RPM_BUILD_ROOT%{_docdir}/xsd/
cp -rL doc/cxx/tree $RPM_BUILD_ROOT%{_docdir}/xsd/
cp -rL doc/cxx/parser $RPM_BUILD_ROOT%{_docdir}/xsd/
cp -rL examples $RPM_BUILD_ROOT%{_docdir}/xsd/

cp -rL doc/xsd.1 $RPM_BUILD_ROOT%{_mandir}/man1/


%clean
rm -rf ${RPM_BUILD_ROOT}

%post

%preun

%files
%defattr(-,root,root)
%{_bindir}/xsd
%{_includedir}/xsd
%doc %{_docdir}/xsd/*
%{_mandir}/man1/xsd.1.gz

%changelog
* Fri Jul 18 2014 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release (4.0.0)

* Tue Apr 27 2010 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Tue Sep 30 2008 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Wed Feb 06 2008 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Tue Jul 31 2007 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Tue Jan 23 2007 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Sep 18 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Jul 10 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon May 01 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Apr 18 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Sun Mar 13 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Sun Feb 05 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Jan 16 2006 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Dec 05 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Tue Nov 08 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Oct 20 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Mon Oct 03 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Fri Sep 16 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Thu Sep 01 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Thu Aug 25 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Sat Aug 06 2005 Boris Kolpackov <boris@codesynthesis.com>
  - new upstream release

* Wed Jul 20 2005 Boris Kolpackov <boris@codesynthesis.com>
  - spec file written for xsd
