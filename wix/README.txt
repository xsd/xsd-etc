This package contains precompiled binaries of CodeSynthesis XSD, a
W3C XML Schema to C++ Data Binding compiler, built for Microsoft
Windows. For more information about XSD visit:

http://www.codesynthesis.com/products/xsd/

This README file describes how to start using XSD in the Microsoft
Windows environment with Visual Studio 2005 (8.0), Visual Studio
2008 (9.0), and Visual Studio 2010 (10.0), Visual Studio 2012 (11.0),
and Visual Studio 2013 (12.0).


Setting up Visual Studio IDE
============================

Before you can start building examples or your applications that use XSD
you need to set include, library and executable search paths in the Visual
Studio IDE. To accomplish this perform the following steps. Here we assume
that you have installed CodeSynthesis XSD into "C:\CodeSynthesis XSD". If
you have it installed in a different place, you will need to adjust the
paths shown below accordingly.

Visual Studio 2005 (8.0) and Visual Studio 2008 (9.0)
-----------------------------------------------------
     
    In the Visual Studio IDE, select "Tools"/"Options"/"Projects and
    Solution"/"VC++ Directories".

    Then, in the "Show directories for" field, select "Include files" and
    create a new entry with the value "C:\CodeSynthesis XSD\include".

    After that, in the "Show directories for" field, select "Library files"
    and create a new entry with the value "C:\CodeSynthesis XSD\lib".

    After that, in the "Show directories for" field, select "Executable files"
    and create a new entry with the value "C:\CodeSynthesis XSD\bin". Make 
    sure it is the first line in the list of directories (use the "Up" 
    button to move the new entry up, if necessary).

    If you are building the 64-bit version of your application, repeat the 
    above steps while selecting x64 in the "Platform" drop-down list in the
    VC++ Directories dialog. Specify "C:\CodeSynthesis XSD\lib64" for the 
    "Library files" entry and specify both "C:\CodeSynthesis XSD\bin64" and
    "C:\CodeSynthesis XSD\bin" for the "Executable files" entry with bin64
    being the first line.

Visual Studio 2010-2013 (10.0-12.0)
-----------------------------------

    1. Open an existing or create a new C++ project (you can open one of
       the example solutions that come with CodeSynthesis XSD).

    2. Open the Property Manager view by selecting "View"->"Property 
       Manager" (or "View"->"Other Windows"->"Property Manager") menu 
       action.

    3. Expand the property hierarchy for the project and find the 
       Microsoft.Cpp.Win32.user property sheet.

    4. Right click on Microsoft.Cpp.Win32.user and select the "Properties"
       menu action.

    5. Select the VC++ Directories tab.

    6. Add the "C:\CodeSynthesis XSD\include" path to the "Include 
       Directories" field (the paths are separated by a semicolon).

    7. Add the "C:\CodeSynthesis XSD\lib" path to the "Library 
       Directories" field.

    8. Add the "C:\CodeSynthesis XSD\bin" path to the "Executable 
       Directories" field and make sure it is the first path in the       
       the list of directories.

    9. Click Ok to close the dialog and then click the Save button at the top 
       of the Property Manager view to save Microsoft.Cpp.Win32.user.

       If you are building the 64-bit version of your application, repeat 
       the above steps for the Microsoft.Cpp.x64.user property sheet. Enter
       "C:\CodeSynthesis XSD\lib64" in the "Library Directories" field and
       enter both "C:\CodeSynthesis XSD\bin64" and "C:\CodeSynthesis XSD\bin"
       in the "Executable Directories" field with bin64 being the first path.

   10. Restart the Visual Studio IDE.


Building Examples
=================

Now you are ready to build XSD examples. Simply open one of the solution 
files from the Start menu and select "Build".


Using XSD in Your Projects
==========================

For various ways to integrate the XSD compiler with the Visual Studio IDE 
as well as other Visual Studio-specific topics, refer to the "Using XSD with
Microsoft Visual Studio" Wiki page:

http://wiki.codesynthesis.com/Using_XSD_with_Microsoft_Visual_Studio
