# Hardcoded version numbers in xsd.wsx:
#
# xerces_c_3_1_1_doc_html_index_html_file
#
XSD_VERSION  := 4.0.0
XERCES_VERSION  := 3.1.1

# Change with each major/minor version change (e.g., 2.3.1 to 2.4.0).
#
PRODUCT_CODE := 5BB83ECA-7A53-41F5-9821-639F79C2D969

# Change with each major/minor version change (e.g., 2.3.1 to 2.4.0).
#
UPGRADE_CODE := FA42588E-3F21-4F31-90C0-E17FF22660AB
