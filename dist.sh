#! /usr/bin/env bash

# Create XSD distribution.
#
# -test
# -j <jobs>
#

trap 'exit 1' ERR

function error ()
{
  echo "$*" 1>&2
}

test=n
jobs=22

while [ $# -gt 0 ]; do
  case $1 in
    -test)
      test=y
      shift
      ;;
    -j)
      shift
      jobs=$1
      shift
      ;;
    *)
      error "unknown option: $1"
      exit 1
      ;;
  esac
done

v=`cat xsd/version`
bv=`cat ../build/build/version`
cv=`cat ../cutl/libcutl/version`
xfv=`cat libxsd-frontend/version`

echo "packaging xsd-$v"
echo "EVERYTHING MUST BE COMMITTED!"

# prepare xsd-x.y.z
#
rm -rf xsd-$v
mkdir xsd-$v
cd xsd
git archive master | tar -x -C ../xsd-$v
cd ..
rm -f xsd-$v/.gitignore

# Copy generated documentation.
#
cd xsd-default/doc
make -j $jobs

files="xsd.1 xsd.xhtml cxx/parser/guide/cxx-parser-guide.* cxx/tree/guide/cxx-tree-guide.* cxx/tree/manual/cxx-tree-manual.*"

for f in $files; do
  rsync -aq $f ../../xsd-$v/doc/$f
  touch ../../xsd-$v/doc/$f
done

cd ../..

# Copy generated source files.
#
cd xsd-default/xsd
make -j $jobs

files="options.?xx cxx/options.?xx cxx/parser/options.?xx cxx/tree/options.?xx"

for f in $files; do
  rsync -aq $f ../../xsd-$v/xsd/$f
  touch ../../xsd-$v/xsd/$f
done

cd ../..

# prepare xsd-x.y.z+dep
#
rm -rf xsd-$v+dep
mkdir xsd-$v+dep
cd xsd+dep
git archive master | tar -x -C ../xsd-$v+dep
cd ../xsd-$v+dep
rm -f .gitignore
rsync -aq --copy-unsafe-links ../xsd-$v/ xsd/
cd ..

# Copy libcult
#
rsync -aq --copy-unsafe-links ../cutl/libcutl-$cv/ xsd-$v+dep/libcutl/

# Copy libxsd-fe
#
rsync -aq --copy-unsafe-links libxsd-frontend-$xfv/ xsd-$v+dep/libxsd-frontend/

# Install build
#
make -C ../build/build-$bv install_inc_dir=`pwd`/xsd-$v+dep install

# Test the dep pack.
#
if [ $test = y ]; then
  rm -rf xsd-$v+dep.test
  rsync -aq --copy-unsafe-links xsd-$v+dep/ xsd-$v+dep.test/
  cd xsd-$v+dep.test
  make -j $jobs test
  cd ..
  rm -r xsd-$v+dep.test
fi

# Package
#
tar cfj xsd-$v.tar.bz2 xsd-$v
tar cfj xsd-$v+dep.tar.bz2 xsd-$v+dep

sha1sum xsd-$v.tar.bz2 >xsd-$v.tar.bz2.sha1
sha1sum xsd-$v+dep.tar.bz2 >xsd-$v+dep.tar.bz2.sha1
